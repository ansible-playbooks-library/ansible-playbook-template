# Ansible Playbook Template

This is a template for Ansible Playbook projects.

It is designed to be the base for new Ansible Playbook projects setting out a folder structure along the lines of Ansible best practices guidelines.

## Directory Layout

The top level of the directory would contain files and directories like so:

````
common/                   
   handlers               # common handlers
   vars                   # common vars

group_vars/               # here we assign variables to particular groups
host_vars/                # if systems need specific variables, put them here

roles/                    # this folder contain the "roles"
(optional)

log/                      # Ansible execution log
tests/                    # playbook tests

playbook.yml              # master playbook
README.md                 # this file :-P
requirements.yml          # role dependencies
````

## Usage

Download or cloning this repo to get the base of a new project.

```
 $ git clone [this-repo]
```

If you clone the repo, remove the ```.git``` folder, init a new project and then add the ```origin``` to the playbook repository.

```
 $ rm -rf .git
 $ git init
 $ git add remote origin add [url-to-the-playbook-repo]
```

Edit as required and update the readme specific to your project.

To create new roles you can use the following command in a terminal within the ```roles``` directory.

```
 $ ansible-galaxy init 'new-role-name'
```

This will create a role folder structure for you to use.

___NOTE:___ Remenber to create the ```roles``` folder previous to execute the previous command.

If you prefer, you can add the roles of the project in the ```requirements.yml``` file.

Ansible best practices can be read here: http://docs.ansible.com/playbooks_best_practices.html

## Running the playbook

Install the dependencies:

```
 $ ansible-galaxy install -r requirements.yml -p roles/
```

Execute the Playbook.

```
 $ ansible-playbook playbook.yml -i hosts -u username -k -v
```

## License

GNU General Public License, versión 3.

## Author Information

* Adrian Novegil <adrian.novegil@gmail.com>
